import com.infinidtech.vtagmanager.CommandGatewayMismatchException;
import com.infinidtech.vtagmanager.ConnectionStatus;
import com.infinidtech.vtagmanager.QueuedCommand;
import com.infinidtech.vtagmanager.SecurityIndication;
import com.infinidtech.vtagmanager.SensorReading;
import com.infinidtech.vtagmanager.SensorStatistic;
import com.infinidtech.vtagmanager.TagMovement;
import com.infinidtech.vtagmanager.VTagAlarm;
import com.infinidtech.vtagmanager.VTagListener;
import com.infinidtech.vtagmanager.VTagManager;
import com.infinidtech.vtagmanager.VTagType;

public class VTagApplication implements VTagListener{
        VTagManager manager;
        public VTagApplication(){
            manager = VTagManager.GetInstance();
            manager.AddListener(VTagApplication.this);
            System.out.println("Connecting to gateway office_gateway");
            manager.AddGateway(VTagType.VTag,"10.0.0.143", 8422, "admin","change","office_gateway", "infinid", false);
        }
        
        public void NewSecurityIndication(SecurityIndication securityIndication) {
            System.out.println("***Got new Security Indication:"+securityIndication.toString());
        }
        
        public void NewSensorReading(SensorReading sensorReading) {
            System.out.println("***Got new Sensor Reading:"+sensorReading.toString());
        }

        public void NewSensorStatistic(SensorStatistic sensorStatistic) {
            System.out.println("***Got new SensorStatistic:"+sensorStatistic.toString());
        }

        public void NewCommandResponse(QueuedCommand queuedCommand) {
            System.out.println("***Got new QueuedCommand Response:"+queuedCommand.toString());
        }

        public void NewAlarm(VTagAlarm alarm) {
            System.out.println("***Got new alarm:"+alarm.toString());
        }

        public void GatewayStatusChanged(ConnectionStatus connectionStatus) {
            System.out.println("***Gateway status changed:"+connectionStatus.toString());
            if(connectionStatus.isConnected())
            {
                try {
                    //First plugin each Fixed tag and then run Set Position on each Fixed tags:
                    //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetPosition("001D76", 337, -711, 1, "infinid");

                    //Next, plug in the asset tags and they will discover their nearest fixed neighbors

                    //Finally, experiment with testing our various commands. Please refer to the SDK Documentation for details.
                    //TAG COMMANDS
                    QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.GetPosition("00789A", "infinid");
                    //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetPosition("005B21", 9,2,55, "infinid");
                    //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.AccelSensor("005B21", false, "infinid");
                    //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.Activate("005B21", "infinid");
                    //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetThreshold("005B21", ThresholdType.Acceleration_Upper_g, 1, 100, "infinid");
                    //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.GetThreshold("005B21", ThresholdType.Acceleration_Upper_g, "infinid");
                    //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.ResetTag("005B21", "infinid");
                    //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetBox("005B21", 5,5,5,5, "infinid");
                    //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetDwellTime("005B21",0, "infinid");
                    //-------GATEWAY COMMANDS--------
                    //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.ResetNetwork("garage_gateway");
                    //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetReportingInterval("garage_gateway", ReportingInterval.ThirtyMinutes);
                    //QueuedCommand queuedCommand = VTagManager.VTagCommandFactory.SetSecurity("garage_gateway", SecurityMode.on, -50);
                    System.out.println(String.format("About to Run command:%s",queuedCommand.toString()));
                    manager.RunCommand(queuedCommand);
                    //java.lang.Thread.sleep(15000);
                    //Can also cancel commands
                    //manager.CancelCommand(queuedCommand.getId());
                    //System.out.println(String.format("Successfully cancelled command? %s",cancelled));
                }
                catch(CommandGatewayMismatchException exc){
                    System.out.println("Command group does not match gateway:"+exc.getMessage());
                }
                catch(Exception exc){
                    System.out.println("Got exception:"+exc.getMessage());
                }
            }
        }

        public void NewTagMovemenent(TagMovement tagMovement) {
            System.out.println("***Got new Tag Movement:"+tagMovement.toString());
        }
    }