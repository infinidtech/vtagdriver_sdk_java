
import com.infinidtech.vtagmanager.CommandGatewayMismatchException;
import com.infinidtech.vtagmanager.ConnectionStatus;
import com.infinidtech.vtagmanager.GPSDiscoveryMode;
import com.infinidtech.vtagmanager.QueuedCommand;
import com.infinidtech.vtagmanager.SecurityIndication;
import com.infinidtech.vtagmanager.SensorReading;
import com.infinidtech.vtagmanager.SensorStatistic;
import com.infinidtech.vtagmanager.TagMovement;
import com.infinidtech.vtagmanager.VTagAlarm;
import com.infinidtech.vtagmanager.VTagListener;
import com.infinidtech.vtagmanager.VTagManager;
import com.infinidtech.vtagmanager.VTagType;

 public class VTagGPSApplication implements VTagListener{
        VTagManager manager;
        public VTagGPSApplication(){
            manager = VTagManager.GetInstance();
            manager.AddListener(this);
            System.out.println("Connecting to gateway office_gateway");
            manager.AddGateway(VTagType.VTagGPS,"192.168.1.19",8422, "admin","change","office_gateway", "infinid", false);   
        }
        
        public void NewSensorReading(SensorReading sensorReading) {
            System.out.println("***Got new Sensor Reading:"+sensorReading.toString());
        }

        public void NewSensorStatistic(SensorStatistic sensorStatistic) {
            System.out.println("***Got new SensorStatistic:"+sensorStatistic.toString());
        }

        public void NewCommandResponse(QueuedCommand queuedCommand) {
            System.out.println("***Got new QueuedCommand Response:"+queuedCommand.toString());
        }

        public void NewAlarm(VTagAlarm alarm) {
            System.out.println("***Got new alarm:"+alarm.toString());
        }

        public void GatewayStatusChanged(ConnectionStatus connectionStatus) {
            System.out.println("***Gateway status changed:"+connectionStatus.toString());
            if(connectionStatus.isConnected())
            {
                try {
                    //TAG COMMANDS
                    //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.GetPosition("005B21", "infinid");
                    QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.SetGPSDiscoveryMode("office_gps_gateway",GPSDiscoveryMode.ImmediatelyUponMovement);
                    //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.AccelSensor("005B21", false, "infinid");
                    //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.Activate("005B21", "infinid");
                    //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.SetThreshold("005B21", ThresholdType.Acceleration_Upper_g, 1, 100, "infinid");
                    //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.GetThreshold("005B21", ThresholdType.Acceleration_Upper_g, "infinid");
                    //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.ResetTag("005B21", "infinid");                    
                    //-------GATEWAY COMMANDS--------
                    //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.ResetNetwork("office_gps_gateway");
                    //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.SetReportingInterval("office_gps_gateway", ReportingInterval.ThirtyMinutes);
                    //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.SetSecurity("office_gps_gateway", SecurityMode.on, -50);
                    //QueuedCommand queuedCommand = VTagManager.VTagGPSCommandFactory.SetGPSDiscoveryMode("office_gps_gateway", GPSDiscoveryMode.After5MinutesInactivity);
                    System.out.println(String.format("About to Run command:%s",queuedCommand.toString()));
                    manager.RunCommand(queuedCommand);
                    //java.lang.Thread.sleep(15000);
                    //Can also cancel commands
                    //manager.CancelCommand(queuedCommand.getId());
                    //System.out.println(String.format("Successfully cancelled command? %s",cancelled));
                }
                catch(CommandGatewayMismatchException exc){
                    System.out.println("Command group does not match gateway:"+exc.getMessage());
                }
                catch(Exception exc){
                    System.out.println("Got exception:"+exc.getMessage());
                }
            }
        }

        public void NewTagMovemenent(TagMovement tagMovement) {
            System.out.println("***Got new Tag Movement:"+tagMovement.toString());
        }
        
        public void NewSecurityIndication(SecurityIndication securityIndication) {
            System.out.println("***Got new Security Indication:"+securityIndication.toString());
        }
    }
