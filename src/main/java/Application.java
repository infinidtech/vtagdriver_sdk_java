import com.infinidtech.vtagmanager.*;
import java.util.Scanner;

public class Application {

    static VTagApplication application;
    static VTagGPSApplication gpsApplication;
    public static void main(String[] args){

        System.out.println("Starting app...");
        //Use this class to test V-Tag hardware
        application = new VTagApplication();
        //gpsApplication = new VTagGPSApplication();
        //Use this class to test V-Tag GPS hardware
        //VTagGPSApplication application = new VTagGPSApplication();
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        System.out.println("Stopping app...");
    }       
}
